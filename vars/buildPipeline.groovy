#!/usr/bin/env groovy

def call(body) {
    pipeline {
        agent any

        stages {
            stage('Always run stage') {
                steps {
                    sh 'echo Always run me'
                }
            }

            body
        }
    }
}
